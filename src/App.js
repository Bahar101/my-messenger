import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Chat from './pages/chat/index';
import styles from './app.module.scss';

export default function App() {
  return (
    <div className={styles['app']}>
      <div className={styles['head']} />
      <div className={styles['main']}>
        <Chat />
      </div>
    </div>
  )
}







// import React from 'react';
// import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
// import Chat from './pages/chat/index';
// import styles from './app.module.scss';

// export default function App() {
//   return (
//     <Router>
//       <div>
//         <nav>
//           <ul>
//             <li>
//               <Link to="/">Home</Link>
//             </li>
//             <li>
//               <Link to="/my-messenger">MyMessenger</Link>
//             </li>
//           </ul>
//         </nav>
//         <Switch>
//           <Route path="/my-messenger">
//             <MyMessenger />
//           </Route>
//           <Route path="/">
//             <Home />
//           </Route>
//         </Switch>
//       </div>
//     </Router>
//   )
// }

// function Home() {
//   return <h2>Home</h2>;
// }

// function MyMessenger() {
//   return (
//     <div className={styles['app']}>
//       <div className={styles['head']} />
//       <div className={styles['main']}>
//         <Chat />
//       </div>
//     </div>
//   )
// }
