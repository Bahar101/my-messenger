import React, { useState, useReducer, useMemo } from 'react';
import AppStatus from './components/appStatus';
import List from './components/list';
import ListItem from './components/listItem';
import ChatDetail from './components/chatDetail';
import Sidebar from './components/sidebar';
import styles from './index.module.scss';
import { INIT_STATE, reducer } from './stateManager/reducer';
import { selectChat, submitMessage, closeChat, searchList } from './stateManager/actionCreator';

export default function Index() {
    const [{ userId, chatList, messages, selectedChatId }, dispatch] = useReducer(reducer, INIT_STATE);
    const [keyword, setKeyword] = useState('');
    const [sidebar, setSidebar] = useState(false);

    const selectedChat = useMemo(
        () => chatList.find(x => x.id === selectedChatId),
        [chatList, selectedChatId]
    );

    const selectedChatMessages = messages.filter(x => x.chatId === selectedChatId);
    const chatListCopy = [...chatList];

    function handleChatSelect(id) {
        dispatch(selectChat(id));
    }

    function handleSubmit(text) {
        dispatch(submitMessage(text));
    }

    function handleClose() {
        dispatch(closeChat());
    }

    function handleSearchList(keyword) {
        setKeyword(keyword);
    }

    function handleSidebar() {
        setSidebar(true);
    }

    return (
        <div className={styles['layout']}>
            <div className={styles['side']}>
                <AppStatus onSubmit={handleSearchList} onSidebar={handleSidebar} />
                <List>
                    {chatList.filter(chat => chat.name.toLowerCase().includes(keyword.toLowerCase()))
                        .map(chat => {
                            const contactMessage = messages.filter(x => x.chatId === chat.id);
                            return <ListItem
                                selected={chat.id === selectedChatId}
                                onSelect={() => handleChatSelect(chat.id)}
                                key={chat.id}
                                name={chat.name}
                                avatar={chat.avatar}
                                time={chat.time}
                                unreadMessageCount={chat.unreadMessageCount}
                                text={contactMessage[contactMessage.length - 1].text}
                            />
                        })}
                </List>
                {sidebar && <Sidebar />}
            </div>
            <div className={styles['main']} onClick={() => setSidebar(false)}>
                {selectedChatId && <ChatDetail
                    onClose={handleClose}
                    selectedChatId={selectedChatId}
                    onSubmit={handleSubmit}
                    avatar={selectedChat.avatar}
                    name={selectedChat.name}
                    messages={selectedChatMessages.map(message => {
                        return {
                            id: message.id,
                            text: message.text,
                            me: message.userId === userId
                        }
                    })}
                />
                }
            </div>
        </div>
    )
}