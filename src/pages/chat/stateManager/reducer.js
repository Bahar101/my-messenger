import { ACTIONS } from './actionCreator';

export const INIT_STATE = {
    userId: '1',
    chatList: [
        { time: '18:05', unreadMessageCount: 13, name: 'Bahare Arab', avatar: '/avatar.png', id: '1' },
        { time: '18:05', unreadMessageCount: 13, name: 'Mohammad Omidi', avatar: '/avatar.png', id: '2' },
        { time: '18:05', unreadMessageCount: 13, name: 'Golbahar Farmanesh', avatar: '/avatar.png', id: '3' },
        { time: '18:05', unreadMessageCount: 14, name: 'Nargess Abbasfar', avatar: '/avatar.png', id: '4' },
    ],
    messages: [
        { chatId: '1', id: '1', text: 'Hi', userId: '1' },
        { chatId: '1', id: '2', text: 'Hi there', userId: '2' },
        { chatId: '2', id: '1', text: 'Hi', userId: '1' },
        { chatId: '2', id: '2', text: 'Hi there', userId: '2' },
        { chatId: '2', id: '3', text: 'Hi there', userId: '2' },
        { chatId: '3', id: '1', text: 'Hi', userId: '1' },
        { chatId: '3', id: '2', text: 'Hi there', userId: '2' },
        { chatId: '3', id: '3', text: 'Hi there', userId: '2' },
        { chatId: '4', id: '1', text: 'Hi there', userId: '2' },
        { chatId: '4', id: '2', text: 'Hi there', userId: '2' },
        { chatId: '4', id: '3', text: 'Hi there', userId: '2' },
        { chatId: '4', id: '4', text: 'Hi there', userId: '2' },
        { chatId: '4', id: '5', text: 'Hi there', userId: '2' },
        { chatId: '4', id: '6', text: 'Hi there', userId: '2' },
        { chatId: '4', id: '7', text: 'Hi there', userId: '2' },
        { chatId: '4', id: '8', text: 'Hi there', userId: '2' },
        { chatId: '4', id: '9', text: 'Hi there', userId: '2' },
        { chatId: '4', id: '10', text: 'Hi there', userId: '2' },
        { chatId: '4', id: '11', text: 'Hi there', userId: '2' },
        { chatId: '4', id: '12', text: 'Hi there', userId: '2' },
        { chatId: '4', id: '13', text: 'Hi there', userId: '2' },
        { chatId: '4', id: '14', text: 'Hi there', userId: '2' },
        { chatId: '4', id: '15', text: 'Hi there', userId: '2' },
    ],
    selectedChatId: null
}

export function reducer(state, action) {
    return (ACTION_HANDLERS[action.type] || (() => state))(state, action.payload)
}

const ACTION_HANDLERS = {
    [ACTIONS.CHAT_SELECTED]: handleChatSelected,
    [ACTIONS.MESSAGE_SUBMITTED]: handleMessageSubmitted,
    [ACTIONS.CHAT_CLOSED]: handleChatClosed
}

function handleChatSelected(state, payload) {
    const selectedChatIndex = state.chatList.findIndex(x => x.id === payload);
    return {
        ...state,
        selectedChatId: payload,
        chatList: [
            ...state.chatList.slice(0, selectedChatIndex),
            {
                ...state.chatList[selectedChatIndex],
                unreadMessageCount: 0
            },
            ...state.chatList.slice(selectedChatIndex + 1)
        ]
    }
}

function handleMessageSubmitted(state, payload) {
    return {
        ...state,
        messages: [
            ...state.messages,
            {
                chatId: state.selectedChatId,
                id: Math.random().toString(),
                text: payload,
                userId: state.userId
            }
        ]
    }
}

function handleChatClosed(state) {
    return {
        ...state,
        selectedChatId: null
    }
}