import React from 'react';
import Avatar from './avatar';
import styles from './listItem.module.scss';

export default function ListItem({ name, text, avatar, time, unreadMessageCount, selected = false, onSelect }) {
    return (
        <div className={styles['list-item'] + ' ' + (selected ? styles[('selected')] : '')} onClick={onSelect}>
            <div className={styles['avatar']}>
                <Avatar name={name} url={avatar} />
            </div>
            <div className={styles['name']}>{name}</div>
            <div className={styles['message']}>{text}</div>
            <div className={styles['time']}>{time}</div>
            <div className={styles['info']}>
                {unreadMessageCount !== 0 && <div>
                    {unreadMessageCount}
                </div>}
            </div>
        </div>
    )
}