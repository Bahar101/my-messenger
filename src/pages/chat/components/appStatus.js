import React, { useState, useEffect, useRef } from 'react';
import TitleBar from './titleBar';
import Sidebar from './sidebar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faSearch, faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import styles from './appStatus.module.scss';

export default function AppStatus({ onSubmit, onSidebar }) {
    const [mode, setMode] = useState('list');
    const [keyword, setKeyword] = useState('');
    const input = useRef(null);

    function handleSubmitSearch(e) {
        setKeyword(e.target.value);
        onSubmit(e.target.value);
    }

    useEffect(
        () => {
            if (mode === 'search') {
                input.current.focus()
            }
            if (mode === 'sidebar') {
                onSidebar()
            }
        },
        [mode]
    );

    return (
        <TitleBar
            first={
                <FontAwesomeIcon
                    icon={mode === 'search' ? faArrowLeft : faBars}
                    size='lg'
                    color='#009588'
                    className={styles['pointer']}
                    onClick={() => {
                        return (
                            <>
                                {mode === 'search' && setMode('list')}
                                {mode === 'list' && setMode('sidebar')}
                            </>
                        )
                    }
                    }
                />}
            middle={
                < div className={styles['app-title']} >
                    {mode === 'list' && "Bahar Messenger!"}
                    {
                        mode === 'search' &&
                        <input
                            type='text'
                            className={styles['search-text']}
                            ref={input}
                            value={keyword}
                            onChange={handleSubmitSearch}
                        />
                    }
                </div >}
            last={mode === 'list' && <FontAwesomeIcon
                icon={faSearch}
                size='lg'
                color='#009588'
                className={styles['pointer']}
                onClick={() => setMode('search')}
            />}
        />
    )
}