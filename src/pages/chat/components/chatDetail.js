import React, { useState, useEffect, useRef } from 'react';
import TitleBar from './titleBar';
import Avatar from './avatar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisV, faTimes, faPaperPlane, faPaperclip } from '@fortawesome/free-solid-svg-icons';
import styles from './chatDetail.module.scss';

export default function ChatDetail({ name, messages, avatar, onSubmit, selectedChatId, onClose }) {
    const [text, setText] = useState('');
    const input = useRef(null);
    const lastEmptyMessage = useRef(null);

    function handleSubmitMessage() {
        if (text !== '') {
            onSubmit(text);
            setText('');
            input.current.focus();
        }
    }

    function handleKeyUp(e) {
        if (e.keyCode === 13) {
            handleSubmitMessage();
        }
    }

    useEffect(
        () => {
            input.current.focus();
            lastEmptyMessage.current.scrollIntoView({ behavior: 'smooth' })
        },
        [selectedChatId, messages]
    );

    return (
        <>
            <TitleBar
                first={<FontAwesomeIcon icon={faTimes} size='lg' color='#009588' className='pointer' onClick={onClose} />}
                middle={
                    <div className={styles['app-title']}>
                        <Avatar name={name} url={avatar} />
                        <div className={styles['name']}>{name}</div>
                    </div>
                }
                last={<FontAwesomeIcon icon={faEllipsisV} size='lg' color='#009588' className='pointer' />}
            />
            <div className={styles['chat-box']}>
                <ul className={styles['messages-panel']}>
                    {
                        messages.map((message, index) => {
                            return <li
                                ref={messages.length === index + 1 ? lastEmptyMessage : null}
                                className={styles[message.me ? 'me' : '']}
                                key={message.id}
                            >
                                {message.text}
                            </li>
                        })
                    }
                </ul>
                <div className={styles['input-section']}>
                    <input
                        ref={input}
                        type='text'
                        value={text}
                        onChange={e => setText(e.target.value)}
                        onKeyUp={handleKeyUp}
                    />
                    <FontAwesomeIcon
                        icon={text !== '' ? faPaperPlane : faPaperclip}
                        color='#009588'
                        size='lg'
                        className={styles['send'] + ' ' + styles['pointer']}
                        onClick={handleSubmitMessage}
                    />
                </div>
            </div>
        </>
    )
}